const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {

// 

   if (request.url === "/" && request.method === "GET") {
    response.writeHead(200, {"Content-Type": "text/plain"} );
    response.end("Welcome to booking system");
  };

// profile REQUEST
  if (request.url === "/profile" && request.method === "GET"){
  	response.writeHead(200, {"Content-Type": "text/plain"});
  	response.end("Welcome to your profile");
  };

// courses REQUEST

  if (request.url === "/courses" && request.method === "GET"){
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Here's our courses available");
  };

// addCourse REQUEST

  if (request.url === "/addCourse" && request.method === "POST"){
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Add course to our resources");
  }

// updateCourse REQUEST

  if (request.url === "/updateCourse" && request.method === "PUT"){
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Update a course to our resources");
  }
  
// archiveCourse REQUEST

  if (request.url === "/archiveCourse" && request.method === "DELETE"){
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Archive courses to our resources");
  }
  
});


server.listen(port);

console.log(`Server now running at port: ${port}`);